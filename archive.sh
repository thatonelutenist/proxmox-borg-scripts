#!/bin/bash
REPO=/rpool/newlab/archive/Websites
REMOTE=pve1.home.mccarty.io/archive/Websites
rclone sync $REPO backup:$REMOTE --transfers=4 --checkers=4 -P
rclone sync $REPO opendrive:$REMOTE --transfers=8 --checkers=8 -P
