#!/bin/bash
export BORG_PASSPHRASE=(passphrase-here)
REPO=/rpool/backup/borg/vma
NAME=vma-$(date -Imin)
BACKUP_DIR=/rpool/backup/containers/dump
REMOTE=pve1.home.mccarty.io/proxmox-borg/vma
borg create -C auto,lzma,1 --list --progress --chunker-params 12,20,15,4095  $REPO::$NAME \
    $BACKUP_DIR --exclude "*.tar*" --exclude "*.log"
borg info $REPO::$NAME --last 1
rclone sync $REPO backup:$REMOTE --transfers=4 --checkers=4 -P
rclone sync $REPO opendrive:$REMOTE --transfers=8 --checkers=8 -P
