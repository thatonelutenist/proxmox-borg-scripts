#! /usr/bin/env ruby
require 'fileutils'

dump_location = "/rpool/backup/containers/dump/"
temp_location = "/rpool/tmp/tmpuntar"
borg_repo_location = "/rpool/backup/borg/lxc"
remote_location = "pve1.home.mccarty.io/proxmox-borg/lxc"

def borg (tar_file, temp_location, borg_repo_location)
  current_dir = Dir.pwd
  FileUtils.mkdir_p temp_location
  Dir.chdir temp_location

  backup_name = File.basename(tar_file, ".tar").gsub("vzdump-lxc-","")

  tar_command = "tar -xf #{tar_file} -C #{Dir.pwd}"

  borg_command = [
    "borg create -C auto,lzma,1 --progress",
    "--exclude \"var/log/lastlog\"",
    "--exclude \"var/cache/*\"",
    "#{borg_repo_location}::#{backup_name} ."
  ].join(" ")

  Process.spawn(tar_command)
  Process.wait
  Process.spawn(borg_command)
  Process.wait

  Dir.chdir current_dir
  FileUtils.rm_rf temp_location
end


ENV["BORG_PASSPHRASE"] = "(PASSPHRASE HERE)"
tar_files = Dir[dump_location + "*.tar"]
tar_files.each{|tar_file|
  puts(tar_file)
  borg(tar_file, temp_location, borg_repo_location)
}

rclone_command = "rclone sync #{borg_repo_location} backup:#{remote_location} --transfers=4 --checkers=4 -P"
Process.spawn(rclone_command)
Process.wait
rclone_command = "rclone sync #{borg_repo_location} opendrive:#{remote_location} --transfers=8 --checkers=8 -P"
Process.spawn(rclone_command)
Process.wait
